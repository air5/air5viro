import React, { Component } from "react";
import { Platform, StyleSheet, Text, View, StatusBar } from "react-native";
import {
  ViroARSceneNavigator,
  ViroARScene,
  ViroText,
  ViroScene,
  ViroCamera,
  ViroARPlaneSelector,
  Viro3DObject,
  ViroAmbientLight,
  ViroBox,
  ViroARPlane,
  ViroSpotLight,
  ViroQuad,
} from "react-viro";

const ArView = () => (
  <ViroARSceneNavigator style={styles.root} initialScene={{ scene: ARScene }} />
);

const ARScene = () => {
  const onTrackingUpdated = (state, reason) => {
    console.log("TRACKING UPDATED");
  };
  const onAnchorUpdated = () => {
    console.log("ANCHOR UPDATED");
  };
  const onError = (event) => {
    console.log("OBJ ERROR: " + event.nativeEvent.error);
  };
  const onTest = () => {
    console.log("END LOAD OBJ");
  };

  return (
    <View>
      <ViroARScene
        onAnchorUpdated={onAnchorUpdated}
        onTrackingUpdated={onTrackingUpdated}
      >
        <ViroARPlane minHeight={0.5} minWidth={0.5} alignment={"Horizontal"}>
          <ViroAmbientLight color="#ffffff" />
          {/* <ViroBox position={[0, 0.25, 0]} scale={[0.5, 0.5, 0.5]} /> */}
          <ViroSpotLight
            color="#ffffff"
            attenuationStartDistance={2}
            attenuationEndDistance={6}
            position={[0, -5, 5]}
            direction={[0 - 1, 0]}
            innerAngle={0}
            outerAngle={45}
          />
          <Viro3DObject
            // source={{uri:"http://masc.cs.gmu.edu/wiki/uploads/ObjViewer/beethoven.obj"}}

            // --CHAIR--
            // source={require("../res/chair/jean_obj.obj")}
            // resources={[
            //   require("../res/chair/jean_obj.mtl"),
            //   require("../res/chair/fabric.jpg"),
            //   require("../res/chair/wood.png"),
            // ]}
            // --SOFA--
            source={require("../res/sofa/sofa_collection.obj")}
            resources={[
              require("../res/sofa/K5109-09.jpg"),
              require("../res/sofa/K5109-07.jpg"),
              require("../res/sofa/K5109-09.jpg"),
              require("../res/sofa/K5110-10.jpg"),
            ]}
            position={[0, 0, 0]}
            scale={[0.001, 0.001, 0.001]}
            type="OBJ"
            onError={onError}
            onLoadEnd={onTest}
          />

          {/* <Viro3DObject
            source={require("../res/emoji_smile/emoji_smile.vrx")}
            resources={[
              require("../res/emoji_smile/emoji_smile_normal.png"),
              require("../res/emoji_smile/emoji_smile_diffuse.png"),
              require("../res/emoji_smile/emoji_smile_specular.png"),
            ]}
            position={[1, 1, 1]}
            scale={[0.004, 0.004, 0.004]}
            type="VRX"
            onError={this.onError}
            onLoadEnd={this.onTest}
          /> */}

          {/* --CHAIR GLTF */}
          {/* <Viro3DObject
            source={require("../res/char_2/scene.gltf")}
            type="GLTF"
            scale={[0.65, 0.65, 0.65]}
            // position={[0.1, 0.3, 0]}
            rotation={[0,210,0]}
            onLoadEnd={this.onTest}
            onError={this.onError}
          /> */}
          <ViroQuad
            rotation={[-90, 0, 0]}
            position={[0, -0.001, 0]}
            width={2.5}
            height={2.5}
            arShadowReceiver={true}
            ignoreEventHandling={true}
          />
        </ViroARPlane>
      </ViroARScene>
    </View>
  );
};

const styles = StyleSheet.create({
  root: {
    flex: 1,
    backgroundColor: "blue",
  },
});

export default ArView;
