import React, { useEffect } from 'react';
import { StyleSheet, View, Text, TouchableOpacity } from 'react-native';
import { useSelector, useDispatch } from 'react-redux';
import { Icon } from 'react-native-elements';
import Colors from '../../constants/Colors';
import ListProducts from '../../components/Home/ListProducts';
import { getRecommendedProducts } from '../../actions/productActions.js';

const Recommend = () => {
  const dispatch = useDispatch();
  const product = useSelector((state) => state.product);

  useEffect(() => {
    dispatch(getRecommendedProducts());
  }, []);

  return (
    <View style={styles.main}>
      <View style={styles.searchSection}>
        <Text
          style={{
            fontSize: 18,
            fontWeight: 'bold',
            color: '#fff'
          }}
        >
          Best Recommend For You
        </Text>
        <TouchableOpacity
          onPress={() => {
            dispatch(getRecommendedProducts());
            console.log('DF2');
          }}
          style={styles.searchIcon}
        >
          <Icon name={'refresh'} size={30} type="material" color={'white'} />
        </TouchableOpacity>
      </View>
      <ListProducts products={product.recommendedProducts} />
    </View>
  );
};

const styles = StyleSheet.create({
  main: {
    flex: 1,
    backgroundColor: Colors.mainBgColor
  },
  searchSection: {
    height: 44,
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
    backgroundColor: Colors.mainColor
  },
  searchIcon: {
    alignSelf: 'center',
    paddingHorizontal: 8
  }
});

export default Recommend;
