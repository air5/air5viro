import { cartActions } from '../constants/ActionTypes';
import { Alert } from 'react-native';

const defaultState = {
  products: []
};

const productReducer = (state = defaultState, action) => {
  switch (action.type) {
    case cartActions.FETCH_CART_PRODUCT_SUCCESS:
      return {
        ...state,
        products: [...action.payload]
      };
    case cartActions.FETCH_CART_PRODUCT_FAILURE:
      Alert.alert('Error', `Can't load cart products, try again`);
      break;
    case cartActions.PUSH_CART_PRODUCT_SUCCESS:
      let changed = false;
      const products = [
        ...state.products.map((pro) => {
          if (pro.productId === action.payload.productId) {
            pro.amount += action.payload.amount;
            changed = true;
          }
          return pro;
        })
      ];
      if (!changed) products.push(action.payload);
      return {
        ...state,
        products: [...products]
      };
    case cartActions.PUSH_CART_PRODUCT_FAILURE:
      Alert.alert('Error', `Can't add product to cart, try again`);
      break;
    case cartActions.REMOVE_CART_PRODUCT_SUCCESS:
      let index = -1;
      const products2 = [...state.products];
      products2.every((product, i) => {
        if (product.productId === action.payload.productId) {
          index = i;
          return false;
        }
        return true;
      });
      products2.splice(index, 1);
      return {
        ...state,
        products: products2
      };
    case cartActions.REMOVE_CART_PRODUCT_FAILURE:
      Alert.alert('Error', `Can't remove product from cart, try again`);
      break;
    case cartActions.UPDATE_CART_PRODUCT_SUCCESS:
      return {
        ...state,
        products: [...action.payload]
      };
    case cartActions.UPDATE_CART_PRODUCT_FAILURE:
      Alert.alert('Error', `Can't update product from cart, try again`);
      break;
    default:
      return { ...state };
  }
};

export default productReducer;
