import { combineReducers } from 'redux';
import productReducer from './productReducer';
import authReducer from './authReducer';
import filterReducer from './filterReducer';
import cartReducer from './cartReducer';
const rootReducer = combineReducers({
  product: productReducer,
  auth: authReducer,
  filter: filterReducer,
  cart: cartReducer
});

export default rootReducer;
