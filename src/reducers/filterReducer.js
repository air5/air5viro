import { filterActions } from '../constants/ActionTypes';
import { Alert } from 'react-native';
const defaultState = {
  currentFilter: {
    category: 'all',
    color: 'all',
    brand: 'all',
    room: 'all',
    minPrice: 1000000,
    maxPrice: 99000000
  },
  dataFilter: {
    category: [],
    brand: [],
    color: [],
    room: [],
    minPrice: 1000000,
    maxPrice: 99000000
  }
};

const productReducer = (state = defaultState, action) => {
  switch (action.type) {
    case filterActions.FETCH_DATA_FILTER_SUCCESS:
      return {
        ...state,
        dataFilter: { ...defaultState.dataFilter, ...action.payload }
      };
    case filterActions.FETCH_DATA_FILTER_FAILURE:
      Alert.alert('Error', `Can't load filter data, try again`);
      break;
    case filterActions.UPDATE_CURRENT_FILTER_SUCCESS:
      return {
        ...state,
        currentFilter: { ...action.payload }
      };
    case filterActions.UPDATE_CURRENT_FILTER_FAILURE:
      Alert.alert('Error', `Failed to update filter, try again`);
      break;
    case filterActions.RESET_FILTER_SUCCESS:
      return {
        ...state,
        currentFilter: { ...defaultState.currentFilter }
      };
    case filterActions.RESET_FILTER_FAILURE:
      Alert.alert('Error', `Failed to reset filter, try again`);
      break;
    default:
      return { ...state };
  }
};

export default productReducer;
