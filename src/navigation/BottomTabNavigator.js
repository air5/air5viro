import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import * as React from 'react';
import TabBarIcon from '../components/TabBarIcon';
import Home from '../screens/HomeTab/Home';
import Cart from '../screens/HomeTab/Cart';
import Profile from '../screens/HomeTab/Profile';
import Recommend from '../screens/HomeTab/Recommend';

const BottomTab = createBottomTabNavigator();

const BottomTabNavigator = () => {
  return (
    <BottomTab.Navigator tabBarOptions={{ showLabel: false }}>
      <BottomTab.Screen
        name="Home"
        component={Home}
        options={{
          tabBarIcon: ({ focused }) => (
            <TabBarIcon focused={focused} name="home-variant-outline" />
          )
        }}
      />
      <BottomTab.Screen
        name="Recommend"
        component={Recommend}
        options={{
          tabBarIcon: ({ focused }) => (
            <TabBarIcon focused={focused} name="fire" />
          )
        }}
      />
      <BottomTab.Screen
        name="Cart"
        component={Cart}
        options={{
          tabBarIcon: ({ focused }) => (
            <TabBarIcon focused={focused} name="truck-fast" />
          )
        }}
      />
      <BottomTab.Screen
        name="Profile"
        component={Profile}
        options={{
          tabBarIcon: ({ focused }) => (
            <TabBarIcon focused={focused} name="account" />
          )
        }}
      />
    </BottomTab.Navigator>
  );
};

export default BottomTabNavigator;
