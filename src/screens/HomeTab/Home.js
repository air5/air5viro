import React, { useEffect, useCallback } from 'react';
import { StyleSheet, View, TouchableOpacity } from 'react-native';
import { useSelector, useDispatch } from 'react-redux';
import { SearchBar, Icon } from 'react-native-elements';
import Colors from '../../constants/Colors';
import ListProducts from '../../components/Home/ListProducts';
import { getProducts, getMoreProducts } from '../../actions/productActions.js';
import { navigation } from '../../rootNavigation';

const Home = () => {
  const dispatch = useDispatch();
  const product = useSelector((state) => state.product);
  const onEndReached = useCallback(() => {
    console.log('END LIST');
    dispatch(getMoreProducts());
  });
  useEffect(() => {
    dispatch(getProducts());
    return () => {
      // cleanup
    };
  }, []);
  return (
    <View style={styles.main}>
      <View style={styles.searchSection}>
        <SearchBar
          platform="android"
          inputContainerStyle={{ height: 23 }}
          containerStyle={{ flex: 1, borderRadius: 3, marginLeft: 10 }}
          leftIconContainerStyle={{ paddingVertical: 0 }}
          inputStyle={{ paddingVertical: 0 }}
          placeholder="Search"
        />
        <TouchableOpacity
          onPress={() => navigation.navigate('Filter')}
          style={styles.searchIcon}
        >
          <Icon
            name={'filter-outline'}
            size={30}
            type="material-community"
            color={'white'}
          />
        </TouchableOpacity>
      </View>
      <ListProducts products={product.products} onEndReached={onEndReached} />
    </View>
  );
};

const styles = StyleSheet.create({
  main: {
    flex: 1,
    backgroundColor: Colors.mainBgColor
  },
  searchSection: {
    height: 59,
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: Colors.mainColor
  },
  searchIcon: {
    alignSelf: 'center',
    paddingHorizontal: 8
  }
});

export default Home;
