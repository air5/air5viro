import axios from 'axios';
import { BASE_URL } from '../constants/Main';
import DeviceInfo from 'react-native-device-info';

export const authHeader = () => {
  const token = 'abc';
  
  if (token) {
    return { 'x-access-token': `${token}`, 'id': DeviceInfo.getUniqueId()};
  } else {
    return null;
  }
};

const callApi = (method, url, payload = {}) => {
  const instance = axios.create({
    baseURL: BASE_URL,
    headers: authHeader()
  });

  switch (method) {
    case 'GET':
      return instance.get(url);
    case 'POST':
      return instance.post(url, payload);
    case 'PUT':
      return instance.put(url, payload);
    case 'PATCH':
      return instance.put(url, payload);
    case 'DEL':
      return instance.delete(url);
  }
};

export default {
  get: (url) => callApi('GET', url),
  post: (url, payload) => callApi('POST', url, payload),
  put: (url, payload) => callApi('PUT', url, payload),
  patch: (url, payload) => callApi('PATCH', url, payload),
  delete: (url) => callApi('DEL', url)
};
