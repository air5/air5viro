import { productActions } from '../constants/ActionTypes';
import productService from '../services/product.service';
import { store } from '../store';

const getFilter = (currentFilter) => {
  const { category, brand, color, room, minPrice, maxPrice } = currentFilter;
  const productAttributes = [];
  if (brand !== 'all') {
    productAttributes.push({
      attribute: 'Thương hiệu',
      value: brand
    });
  }
  if (color !== 'all') {
    productAttributes.push({
      attribute: 'Màu sắc',
      value: color
    });
  }
  const filter = {
    room: room === 'all' ? '' : room,
    category: category === 'all' ? '' : category,
    productAttributes,
    minPrice,
    maxPrice
  };
  return filter;
};

export const getProducts = () => async (dispatch) => {
  const currentFilter = store.getState().filter.currentFilter;
  const currentProduct = store.getState().product;
  const { pageSize } = currentProduct;
  const filter = getFilter(currentFilter);
  productService
    .getProducts(pageSize, 0, filter)
    .then((response) => {
      dispatch({
        type: productActions.FETCH_PRODUCT_LIST_WITH_FILTER_SUCCESS,
        products: response.data.data,
        hasNext: response.data.hasNext
      });
    })
    .catch((error) => {
      console.log(error);
      dispatch({
        type: productActions.FETCH_PRODUCT_LIST_WITH_FILTER_FAILURE,
        error
      });
    });
};

export const getMoreProducts = () => (dispatch) => {
  const currentProduct = store.getState().product;
  const { pageSize, pageNumber, hasNext } = currentProduct;
  if (!hasNext) return;
  const currentFilter = store.getState().filter.currentFilter;
  const filter = getFilter(currentFilter);
  productService
    .getProducts(pageSize, pageNumber, filter)
    .then((response) => {
      dispatch({
        type: productActions.FETCH_MORE_PRODUCT_LIST_WITH_FILTER_SUCCESS,
        products: response.data.data,
        hasNext: response.data.hasNext
      });
    })
    .catch((error) => {
      console.log(error);
      dispatch({
        type: productActions.FETCH_MORE_PRODUCT_LIST_WITH_FILTER_FAILURE,
        error
      });
    });
};

export const getRecommendedProducts = () => (dispatch) => {
  productService
    .getRecommendedProducts(20, 0, {})
    .then((response) => {
      dispatch({
        type: productActions.FETCH_RECOMMENDED_PRODUCT_SUCCESS,
        recommendedProducts: response.data.data
      });
    })
    .catch((error) => {
      console.log(error);
      dispatch({
        type: productActions.FETCH_RECOMMENDED_PRODUCT_FAILURE,
        error
      });
    });
};
