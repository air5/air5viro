import React, { useCallback } from 'react';
import {
  View,
  StyleSheet,
  Text,
  Image,
  FlatList,
  TouchableOpacity
} from 'react-native';
import { navigation } from '../../rootNavigation';
import { Rating } from 'react-native-ratings';

const formatCurrency = (money) =>
  String(money).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.');

const ProductItem = ({ item }) => {
  const openDetail = useCallback(() => {
    navigation.navigate('ProductDetail', { id: item.id });
  });
  let discount = Math.round((item.discountPrice / item.originalPrice) * 100);
  return (
    <TouchableOpacity style={[styles.item]} onPress={openDetail}>
      <Image
        source={{ uri: item.image }}
        style={styles.itemImage}
        resizeMode="contain"
      />
      <Text style={styles.itemTitle}>{item.name}</Text>
      <View style={[styles.inlineItems]}>
        <Rating readonly={true} imageSize={12} startingValue={2} />
        <Text style={styles.ratingCount}>(50)</Text>
      </View>
      <View style={[styles.inlineItems]}>
        <Text style={styles.itemPrice}>
          {formatCurrency(item.originalPrice)}₫
        </Text>
        <Text style={styles.itemDiscount}>-{discount}%</Text>
      </View>
    </TouchableOpacity>
  );
};

const ListProducts = (props) => {
  return (
    <FlatList
      data={props.products}
      renderItem={({ item }) => <ProductItem item={item} />}
      numColumns={2}
      keyExtractor={(item) => item.id}
      onEndReachedThreshold={0.04}
      onEndReached={props.onEndReached}
    />
  );
};

const styles = StyleSheet.create({
  item: {
    flex: 1,
    flexDirection: 'column',
    margin: 1,
    padding: 9,
    borderRadius: 2,
    backgroundColor: 'white'
  },
  itemImage: {
    width: 180,
    height: 180
  },
  itemTitle: {
    fontSize: 13,
    marginTop: 8
  },
  ratingCount: {
    fontSize: 12,
    paddingLeft: 5,
    color: 'gray'
  },
  itemPrice: {
    fontSize: 16,
    fontWeight: 'bold',
    color: 'black'
  },
  itemDiscount: {
    fontSize: 15,
    color: 'gray',
    marginLeft: 8
  },
  inlineItems: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    marginTop: 4
  }
});

export default ListProducts;
