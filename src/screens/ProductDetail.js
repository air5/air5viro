import React, { useState, useEffect, useCallback } from 'react';
import {
  StyleSheet,
  View,
  Text,
  Image,
  Button,
  ScrollView,
  FlatList,
  TouchableOpacity,
} from 'react-native';
import { useSelector, useDispatch } from 'react-redux';
import { Rating } from 'react-native-ratings';
import { getProducts } from '../actions/productActions.js';
import { navigation } from '../rootNavigation';
import productService from '../services/product.service';

const formatCurrency = (money) =>
  String(money).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.');
const ProductDetail = ({ route }) => {
  const [product, setProduct] = useState();
  console.log(route.params.id);
  useEffect(() => {
    productService
      .getProductDetail(route.params.id)
      .then((response) => {
        setProduct(response.data);
        console.log(response.data);
        console.log(product);
      })
      .catch((error) => {
        console.log(error);
      });
  }, []);
  const openArView = useCallback(() => {
    navigation.navigate('ArView');
  });
  if (product) {
    if (product.originalPrice !== 0) {
      let discount = Math.round(
        (product.discountPrice / product.originalPrice) * 100
      );
      return (
        <ScrollView>
          <View style={styles.item}>
            <Image
              source={{ uri: product.image }}
              style={styles.itemImageDetail}
              resizeMode="contain"
            />
            <View>
              <Text style={styles.itemDetailName}>{product.name}</Text>
            </View>
            <View style={[styles.inlineItems]}>
              <Rating readonly={true} imageSize={20} startingValue={2} />
              <Text style={styles.ratingCount}>(50)</Text>
            </View>
            <View style={styles.inlineItems}>
              <Text style={styles.priceHeader}>GIÁ:</Text>
              <Text style={styles.itemPriceDetail}>
                {' '}
                {formatCurrency(product.discountPrice)}đ{' '}
              </Text>
              <Text style={styles.originalPrice}>
                {formatCurrency(product.originalPrice)} đ
              </Text>
              <Text style={styles.itemDiscount}> -{discount}%</Text>
            </View>
            <Text></Text>
            <Button style={styles.buyButton} title="Mua hết" />
          </View>
          <View style={styles.item}>
            <Button
              title="Thử xem bên ngoài thế nào? (AR)"
              color="red"
              style={styles.arButton}
              onPress={openArView}
            />
          </View>
          <View style={styles.item}>
            <View style={styles.inlineItems}>
              <Text style={styles.titleHeader}>Phân loại: </Text>
              <Text style={styles.informData}>{product.category}</Text>
            </View>
          </View>
          <View style={styles.item}>
            <View style={styles.inlineItems}>
              <Text style={styles.titleHeader}>Loại phòng: </Text>
              <Text style={styles.informData}>{product.room}</Text>
            </View>
          </View>
          <View style={styles.item}>
            <Text style={styles.titleHeader}>Thông tin chi tiết</Text>
            <FlatList
              data={product.productAttributes}
              renderItem={({ item }) => (
                <RenderDetail item={item} />
              )}
            />
          </View>
          <View style={styles.item}>
            <Text style={styles.titleHeader}>Có thể bạn quan tâm:</Text>
            <ListRecommend />
          </View>

        </ScrollView>
      );
    } else if (product.originalPrice === 0) {
      return (
        <ScrollView>
          <View style={styles.item}>
            <Image
              source={{ uri: product.image }}
              style={styles.itemImageDetail}
              resizeMode="contain"
            />
            <View>
              <Text style={styles.itemDetailName}>{product.name}</Text>
            </View>
            <View style={[styles.inlineItems]}>
              <Rating readonly={true} imageSize={20} startingValue={2} />
              <Text style={styles.ratingCount}>(50)</Text>
            </View>
            <View style={styles.inlineItems}>
              <Text style={styles.priceHeader}>GIÁ</Text>
              <Text style={styles.itemPriceDetail}>{' '}Tham khảo ở web trước!</Text>
            </View>
            <Text></Text>
            <Button style={styles.buyButton} title="Trở lại trang web" />
          </View>
          <View style={styles.item}>
            <Button
              title="Thử xem bên ngoài thế nào? (AR)"
              color="red"
              style={styles.arButton}
              onPress={openArView}
            />
          </View>
          <View style={styles.item}>
            <View style={styles.inlineItems}>
              <Text style={styles.titleHeader}>Phân loại: </Text>
              <Text style={styles.informData}>{product.category}</Text>
            </View>
          </View>
          <View style={styles.item}>
            <View style={styles.inlineItems}>
              <Text style={styles.titleHeader}>Loại phòng: </Text>
              <Text style={styles.informData}>{product.room}</Text>
            </View>
          </View>
          <View style={styles.item}>
            <Text style={styles.titleHeader}>Thông tin chi tiết</Text>
            <FlatList
              data={product.productAttributes}
              renderItem={({ item }) => (
                <RenderDetail item={item} />
              )}
            />
          </View>
          <ListRecommend />
        </ScrollView>
      );
    }
  } else {
    return <Text></Text>;
  }
};

let check = 1;
const RenderDetail = ({ item }) => {
  check = check * -1;
  if (check == 1) {
    return (
      <View style={styles.inlineItems}>
        <Text style={styles.informHead}> {item.attribute}</Text>
        <Text style={styles.informData}>{item.value}</Text>
      </View>
    );
  } else if (check == -1) {
    return (
      <View style={styles.inlineItems1}>
        <Text style={styles.informHead}> {item.attribute}</Text>
        <Text style={styles.informData}>{item.value}</Text>
      </View>
    );
  }
};

const ListRecommend = () => {
  const dispatch = useDispatch();
  const product = useSelector((state) => state.product);
  const onEndReached = useCallback(() => {
    dispatch(getProducts());
  });
  useEffect(() => {
    dispatch(getProducts());
    return () => {
      // cleanup
    };
  }, []);
  return (
    <FlatList
      horizontal={true}
      data={product.products}
      renderItem={({ item }) => <ProductItem item={item} />}
      keyExtractor={(item) => item.id}
      onEndReachedThreshold={0.04}
      onEndReached={onEndReached}
    />
  );
};

const ProductItem = ({ item }) => {
  let discount = Math.round((item.discountPrice / item.originalPrice) * 100);
  return (
    <TouchableOpacity style={[styles.item]}>
      <Image
        source={{ uri: item.image }}
        style={styles.itemImage}
        resizeMode="contain"
      />
      <Text style={styles.itemTitle}>{item.name}</Text>
      <View style={[styles.inlineItems]}>
        <Rating readonly={true} imageSize={12} startingValue={2} />
        <Text style={styles.ratingCount}>(50)</Text>
      </View>
      <View style={[styles.inlineItems]}>
        <Text style={styles.itemPrice}>
          {formatCurrency(item.originalPrice)}₫
        </Text>
        <Text style={styles.itemDiscount}>-{discount}%</Text>
      </View>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  item: {
    flex: 1,
    flexDirection: 'column',
    margin: 1,
    padding: 9,
    borderRadius: 2,
    backgroundColor: 'white',
    marginTop: 10
  },
  itemImage: {
    width: 180,
    height: 180
  },
  itemImageDetail: {
    width: 390,
    height: 390,
    justifyContent: 'center',
    alignItems: 'center'
  },
  itemTitle: {
    fontSize: 13,
    marginTop: 8
  },
  ratingCount: {
    paddingLeft: 5,
    fontSize: 12,
    color: 'gray'
  },
  itemPrice: {
    fontSize: 16,
    fontWeight: 'bold',
    color: 'black'
  },
  priceHeader: {
    fontSize: 25,
    fontWeight: 'bold',
    color: 'black',
    textDecorationLine: 'underline'
  },
  itemPriceDetail: {
    fontSize: 25,
    fontWeight: 'bold',
    color: '#0ed145',
    textAlignVertical: 'bottom'
  },
  originalPrice: {
    fontSize: 20,
    color: 'gray',
    textDecorationLine: 'line-through',
    textAlignVertical: 'bottom'
  },
  itemDiscount: {
    fontSize: 20,
    color: 'gray',
    marginLeft: 8,
    textAlignVertical: 'bottom'
  },
  inlineItems: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    marginTop: 4,
    textAlignVertical: 'center'
  },
  inlineItems1: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    marginTop: 4,
    backgroundColor: '#f4f4f4',
    textAlignVertical: 'center'
  },
  itemDetailName: {
    fontSize: 20,
    marginTop: 10,
    fontWeight: 'bold'
  },
  titleHeader: {
    fontSize: 18,
    marginTop: 10,
    fontWeight: 'bold'
  },
  inlineItem: {
    flexDirection: 'row',
    flexWrap: 'wrap'
  },
  informHead: {
    fontSize: 16,
    marginTop: 10,
    color: '#585858',
    width: 200,
    alignItems: 'center'
  },
  informData: {
    fontSize: 16,
    marginTop: 10,
    color: 'black',
    width: 200,
    alignItems: 'center'
  },
  buyButton: {
    backgroundColor: 'green'
  },
  arButton: {}
});

export default ProductDetail;
