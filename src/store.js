import { createStore, applyMiddleware, compose } from 'redux';
import reducer from './reducers';
import thunk from 'redux-thunk';
import AsyncStorage from '@react-native-community/async-storage';
import { persistReducer, persistStore } from 'redux-persist';

const middlewares = [thunk];
const enhancers = [applyMiddleware(...middlewares)];
const persistConfig = {
  key: 'root',
  storage: AsyncStorage,
  whiteList: ['user', 'cart']
};

const persistedReducer = persistReducer(persistConfig, reducer);

export const store = createStore(persistedReducer, compose(...enhancers));
export const persistor = persistStore(store);