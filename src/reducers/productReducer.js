import { productActions } from '../constants/ActionTypes';

const defaultState = {
  products: [],
  recommendedProducts: [],
  pageSize: 20,
  pageNumber: 0,
  hasNext: true,
};

const productReducer = (state = defaultState, action) => {
  switch (action.type) {
    case productActions.FETCH_PRODUCT_LIST_WITH_FILTER_SUCCESS:
      return {
        ...state,
        products: action.products,
        pageNumber: 1,
        hasNext: action.hasNext
      };

    case productActions.FETCH_PRODUCT_LIST_WITH_FILTER_FAILURE:
      return {
        ...state
      };

    case productActions.FETCH_MORE_PRODUCT_LIST_WITH_FILTER_SUCCESS:
      return {
        ...state,
        products: state.products.concat(action.products),
        pageNumber: state.pageNumber + 1,
        hasNext: action.hasNext
      };

    case productActions.FETCH_MORE_PRODUCT_LIST_WITH_FILTER_FAILURE:
      return {
        ...state
      };

    case productActions.RESET_PRODUCT_PAGING:
      return {
        ...state,
        pageNumber: 0
      };

    case productActions.FETCH_RECOMMENDED_PRODUCT_SUCCESS:
      return {
        ...state,
        recommendedProducts: action.recommendedProducts
      };

    case productActions.FETCH_RECOMMENDED_PRODUCT_FAILURE:
      return {
        ...state
      };

    default:
      return { ...state };
  }
};

export default productReducer;
