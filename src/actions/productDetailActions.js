import { productActions } from '../constants/ActionTypes';
import productService from '../services/product.service';

export const getProductDetail = (id) => (dispatch) => {
    productService
        .getProductDetail(id)
        .then((response) => {
            dispatch({
                type: productActions.FETCH_PRODUCT_DETAIL_SUCCESS,
                productsDetail: response.data
            });
        })
        .catch((error) => {
            dispatch({
                type: productActions.FETCH_PRODUCT_DETAIL_FAILURE,
                error
            });
        });
};