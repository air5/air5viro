import React from 'react';
import { Provider } from 'react-redux';
import { store, persistor } from './src/store';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator, TransitionPresets } from '@react-navigation/stack';
import { PersistGate } from 'redux-persist/integration/react';
import { navigationRef } from './src/rootNavigation';
import BottomTabNavigator from './src/navigation/BottomTabNavigator';
import screens from './src/screens';

const Stack = createStackNavigator();

const App = () => {
  return (
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        <NavigationContainer ref={navigationRef}>
          <Stack.Navigator
            screenOptions={{
              gestureEnabled: false,
              headerShown: false
            }}
          >
            <Stack.Screen component={BottomTabNavigator} name="Root" />
            <Stack.Screen component={screens.Login} name="Login" />
            <Stack.Screen component={screens.Register} name="Register" />
            <Stack.Screen component={screens.ProductDetail} name="ProductDetail" />
            <Stack.Screen component={screens.ArView} name="ArView" />
            <Stack.Screen options={{
              cardStyle: {
                backgroundColor: 'transparent'
              },
              ...TransitionPresets.FadeFromBottomAndroid
            }} component={screens.Filter} name="Filter" />
          </Stack.Navigator>
        </NavigationContainer>
      </PersistGate>
    </Provider>
  );
};

export default App;
