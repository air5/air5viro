import { filterActions, productActions } from '../constants/ActionTypes';
import axiosInstance from '../helpers/axiosInstance';
import { getProducts } from './productActions';
export const fetchDataFilter = () => {
    return async dispatch => {
        try {
            const rq = await axiosInstance.post('/products/meta', {
                query: "",
                room: "",
                category: "",
                productAttributes: [
                ]
            });
            const data = rq.data;
            const filter = {};
            filter.room = data.rooms.map(room => ({
                name: room._id,
                value: room._id,
            }));
            filter.category = data.categories.map(cat => ({
                name: cat._id,
                value: cat._id,
            }));
            filter.color = [];
            filter.brand = [];
            data.values.map(value => {
                if (value._id.attribute === 'Thương hiệu') {
                    filter.brand.push({
                        name: value._id.value,
                        value: value._id.value,
                    });
                }
                if (value._id.attribute === 'Màu sắc') {
                    filter.color.push({
                        name: value._id.value,
                        value: value._id.value,
                    });
                }
            });
            dispatch({
                payload: filter,
                type: filterActions.FETCH_DATA_FILTER_SUCCESS
            });
        }
        catch (e) {
            console.warn(e);
            dispatch({
                type: filterActions.FETCH_DATA_FILTER_FAILURE
            });
        }
    };
};
export const updateCurrentFilter = (category, brand, color, room, minPrice, maxPrice) => {
    return async dispatch => {
        try {
            const data = {
                room,
                category,
                color,
                brand,
                minPrice,
                maxPrice
            };
            await dispatch({
                payload: data,
                type: filterActions.UPDATE_CURRENT_FILTER_SUCCESS
            });
            await dispatch(getProducts());
        }
        catch (e) {
            console.warn(e);
            dispatch({
                type: filterActions.UPDATE_CURRENT_FILTER_FAILURE
            });
        }
    };
};
export const resetFilter = () => {
    return async dispatch => {
        try {
            await dispatch({
                type: filterActions.RESET_FILTER_SUCCESS
            });
            await dispatch(getProducts());
        }
        catch (e) {
            console.warn(e);
            dispatch({
                type: filterActions.RESET_FILTER_FAILURE
            });
        }
    };
};