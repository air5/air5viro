import React from 'react';
import { Text, View, TouchableOpacity } from 'react-native';
import { navigation } from '../rootNavigation';

const Login = () => {
  return (
    <View>
      <TouchableOpacity onPress={() => navigation.navigate('ArView')}>
        <Text>Touch to navigate to Register</Text>
      </TouchableOpacity>
      <TouchableOpacity onPress={() => navigation.navigate('BottomTabNavigator')}>
        <Text>Touch to navigate to Home</Text>
      </TouchableOpacity>
    </View>
  );
};

export default Login;
