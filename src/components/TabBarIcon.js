import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import * as React from 'react';

import Colors from '../constants/Colors';

const TabBarIcon = (props) => {
  return (
    <Icon
      name={props.name}
      size={30}
      color={props.focused ? Colors.tabIconSelected : Colors.tabIconDefault}
    />
  );
};

export default TabBarIcon;
