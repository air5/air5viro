import React from 'react';
import { Text, View, TouchableOpacity } from 'react-native';
import { navigation } from '../rootNavigation';

const Register = () => {
  return (
    <View>
      <TouchableOpacity
        onPress={() => {
          navigation.navigate('Login');
        }}
      >
        <Text>Back</Text>
      </TouchableOpacity>
    </View>
  );
};

export default Register;
