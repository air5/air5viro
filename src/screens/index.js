import Login from './Login';
import Register from './Register';
import ProductDetail from './ProductDetail';
import ArView from './ArView';
import Filter from './Filter';
export default ({
    Login,
    Register,
    ProductDetail,
    ArView,
    Filter
});