import React, { useEffect } from 'react';
import {
  StyleSheet,
  Text,
  View,
  ScrollView,
  Image,
  TouchableOpacity,
  SafeAreaView
} from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import {
  fetchCartProducts,
  PushCartProduct,
  RemoveCartProduct
} from '../../actions/cartActions';
import { useIsFocused } from '@react-navigation/native';
import 'intl';
import 'intl/locale-data/jsonp/vi-VN';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import window from '../../constants/Layout';
import Colors from '../../constants/Colors';

export const numberFormat = (number) =>
  new Intl.NumberFormat('vi-VN').format(number);

const Cart = () => {
  const focused = useIsFocused();
  const dispatch = useDispatch();
  const cart = useSelector((state) => state.cart);
  useEffect(() => {
    if (focused) {
      dispatch(fetchCartProducts());
    }
    return () => {};
  }, [focused]);
  const _onIncreaseAmount = (productId) => {
    dispatch(PushCartProduct(productId, 1));
  };
  const _onDecreaseAmount = (productId) => {
    dispatch(PushCartProduct(productId, -1));
  };
  const _onRemoveCartProduct = (productId) => {
    dispatch(RemoveCartProduct(productId));
  };
  const _onOrder = () => {};
  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.cartOptions}>
        <View
          style={{
            height: 50,
            flexDirection: 'row',
            paddingHorizontal: 10,
            alignItems: 'center',
            borderBottomColor: '#ddd',
            borderBottomWidth: 1,
            justifyContent: 'space-between'
          }}
        >
          <View>
            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
              <Icon name="truck" size={20} color="green" />
              <Text
                style={{
                  color: '#666',
                  fontSize: 16,
                  fontWeight: 'bold',
                  marginLeft: 5
                }}
              >
                Phí vận chuyển
              </Text>
            </View>
            <Text
              style={{
                fontSize: 12,
                color: '#666'
              }}
            >
              Miễn phí vận chuyển khi đạt giá trị tối thiểu.
            </Text>
          </View>
          <Text
            style={{
              color: 'green',
              fontSize: 16,
              fontWeight: 'bold',
              marginLeft: 5
            }}
          >
            {numberFormat(100000)}đ
          </Text>
        </View>
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'space-between'
          }}
        >
          <View
            style={{
              height: 40,
              margin: 5
            }}
          >
            <Text
              style={{
                fontWeight: 'bold',
                color: '#666',
                fontSize: 14
              }}
            >
              Tổng tiền
            </Text>
            <Text
              style={{
                color: 'red',
                fontWeight: 'bold',
                fontSize: 16
              }}
            >
              {numberFormat(
                cart.products.reduce(
                  (a, b) => (a += b.amount * b.product.discountPrice),
                  0
                )
              )}
              đ
            </Text>
          </View>
          <TouchableOpacity
            onPress={_onOrder}
            style={{
              backgroundColor: Colors.mainColor,
              height: 50,
              justifyContent: 'center',
              paddingHorizontal: 10
            }}
          >
            <Text
              style={{
                fontSize: 18,
                color: '#fff',
                fontWeight: 'bold'
              }}
            >
              Order Now
            </Text>
          </TouchableOpacity>
        </View>
      </View>
      <ScrollView showsVerticalScrollIndicator={false}>
        <View
          style={{
            backgroundColor: Colors.mainColor,
            height: 44,
            justifyContent: 'center',
            alignItems: 'center'
          }}
        >
          <Text
            style={{
              fontSize: 18,
              fontWeight: 'bold',
              color: '#fff'
            }}
          >
            Giỏ hàng
          </Text>
        </View>
        {cart.products.map((product, index) => (
          <TouchableOpacity key={index} style={styles.cartItem}>
            <Image
              style={{
                height: 80,
                width: 80
              }}
              source={{ uri: product.product.image }}
            />
            <View
              style={{
                width: window.window.width - 80 - 20 - 30
              }}
            >
              <Text
                numberOfLines={1}
                style={{
                  fontSize: 16,
                  fontWeight: 'bold'
                }}
              >
                {product.product.name}
              </Text>
              <View
                style={{
                  flexDirection: 'row',
                  marginVertical: 10,
                  marginTop: 5
                }}
              >
                <Text
                  style={{
                    fontWeight: 'bold',
                    textDecorationLine: 'line-through',
                    textDecorationStyle: 'dashed',
                    color: '#666'
                  }}
                >
                  {numberFormat(product.product.originalPrice)}đ
                </Text>
                <Text
                  style={{
                    marginLeft: 5,
                    fontWeight: 'bold',
                    color: 'red',
                    textDecorationLine: 'none'
                  }}
                >
                  {numberFormat(product.product.discountPrice)}đ
                </Text>
              </View>
              <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                <TouchableOpacity
                  onPress={() =>
                    product.amount > 1
                      ? _onDecreaseAmount(product.productId)
                      : Function
                  }
                  style={{
                    ...styles.btnAmount,
                    marginLeft: 0.5
                  }}
                >
                  <Text
                    style={{
                      fontWeight: 'bold',
                      fontSize: 20
                    }}
                  >
                    -
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity
                  activeOpacity={1}
                  style={{
                    ...styles.btnAmount,
                    width: 50
                  }}
                >
                  <Text style={{ fontWeight: 'bold' }}>{product.amount}</Text>
                </TouchableOpacity>
                <TouchableOpacity
                  onPress={() => _onIncreaseAmount(product.productId)}
                  style={styles.btnAmount}
                >
                  <Text
                    style={{
                      fontSize: 20
                    }}
                  >
                    +
                  </Text>
                </TouchableOpacity>
              </View>
            </View>
            <TouchableOpacity
              onPress={() => _onRemoveCartProduct(product.productId)}
              style={{
                width: 30,
                height: 30,
                justifyContent: 'center',
                alignItems: 'center'
              }}
            >
              <Icon name="trash-can-outline" size={24} color="red" />
            </TouchableOpacity>
          </TouchableOpacity>
        ))}
        <View style={styles.separater}>
          <View
            style={{
              position: 'absolute',
              backgroundColor: 'rgb(242,242,242)',
              width: 150,
              height: 20,
              justifyContent: 'center',
              alignItems: 'center',
              left: (window.window.width - 150) / 2,
              top: (2 - 20) / 2
            }}
          >
            <Text
              style={{
                fontWeight: '500',
                color: '#666'
              }}
            >
              Có thể bạn cũng thích
            </Text>
          </View>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

export default Cart;

const styles = StyleSheet.create({
  container: {
    width: '100%',
    height: '100%'
  },
  cartOptions: {
    backgroundColor: '#fff',
    borderTopColor: '#ddd',
    borderTopWidth: 0.5,
    borderBottomColor: '#ddd',
    borderBottomWidth: 1,
    height: 100,
    position: 'absolute',
    bottom: 0,
    left: 0,
    width: '100%'
  },
  cartItem: {
    backgroundColor: '#fff',
    flexDirection: 'row',
    padding: 10,
    paddingVertical: 15,
    alignItems: 'center',
    borderBottomColor: '#ddd',
    borderBottomWidth: 0.5
  },
  btnAmount: {
    height: 30,
    width: 30,
    justifyContent: 'center',
    alignItems: 'center',
    borderColor: '#ddd',
    borderWidth: 0.5
  },
  separater: {
    position: 'relative',
    marginVertical: 20,
    backgroundColor: '#ddd',
    height: 2
  }
});
