import React, { useState, useRef, useEffect } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Animated
} from 'react-native';
import { navigation } from '../rootNavigation';
import { PanGestureHandler, State } from 'react-native-gesture-handler';
import window from '../constants/Layout';
import { Picker } from '@react-native-community/picker';
import RangeSlider from 'rn-range-slider';
import Colors from '../constants/Colors';
import { useSelector, useDispatch } from 'react-redux';
import {
  fetchDataFilter,
  updateCurrentFilter,
  resetFilter
} from '../actions/filterActions';
const Filter = () => {
  const dispatch = useDispatch();
  const dataFilter = useSelector((state) => state.filter.dataFilter);
  const currentFilter = useSelector((state) => state.filter.currentFilter);
  const [category, setCategory] = useState(currentFilter.category);
  const [brand, setBrand] = useState(currentFilter.brand);
  const [color, setColor] = useState(currentFilter.color);
  const [room, setRoom] = useState(currentFilter.room);
  const [minPrice, setMinPrice] = useState(currentFilter.minPrice);
  const [maxPrice, setMaxPrice] = useState(currentFilter.maxPrice);
  const timeoutRef = useRef(null);
  const rangeRef = useRef(null);
  const _bottomOffset = new Animated.Value(0);
  useEffect(() => {
    setBrand(currentFilter.brand);
    setColor(currentFilter.color);
    setRoom(currentFilter.room);
    setCategory(currentFilter.category);
    setMinPrice(currentFilter.minPrice);
    setMaxPrice(currentFilter.maxPrice);
    return () => {};
  }, [currentFilter]);
  const _onGestureEvent = ({ nativeEvent: { translationY } }) => {
    if (translationY > 0) {
      _bottomOffset.setValue(-translationY);
    }
  };
  const _rangeRefHandler = (component) => {
    rangeRef.current = component;
  };
  const _onHandlerStateChange = ({ nativeEvent }) => {
    if (nativeEvent.state === State.END) {
      if (nativeEvent.translationY > window.window.height * 0.5) {
        Animated.timing(_bottomOffset, {
          duration: 100,
          toValue: -window.window.height * 0.8,
          useNativeDriver: false
        }).start(() => navigation.goBack());
      } else {
        Animated.timing(_bottomOffset, {
          duration: 300,
          toValue: 0,
          useNativeDriver: false
        }).start();
      }
    }
  };
  const _onRangePriceChange = (low, high, fromUser) => {
    clearTimeout(timeoutRef.current);
    timeoutRef.current = setTimeout(() => {
      if (fromUser) {
        setMinPrice(low);
        setMaxPrice(high);
      }
    }, 200);
  };
  // fetch data
  useEffect(() => {
    if (dataFilter.brand.length === 0) {
      dispatch(fetchDataFilter());
    }
    return () => {
      // cleanup
    };
  }, [dataFilter]);
  const _onSubmit = () => {
    (async () => {
      await dispatch(
        updateCurrentFilter(category, brand, color, room, minPrice, maxPrice)
      );
      navigation.goBack();
    })();
  };
  const _onReset = () => {
    dispatch(resetFilter());
  };
  return (
    <View style={styles.container}>
      <TouchableOpacity
        activeOpacity={1}
        onPress={() => {
          navigation.goBack();
        }}
        style={styles.backdrop}
      ></TouchableOpacity>

      <Animated.View
        style={{
          ...styles.mainFilterWrapper,
          bottom: _bottomOffset
        }}
      >
        <PanGestureHandler
          onGestureEvent={_onGestureEvent}
          onHandlerStateChange={_onHandlerStateChange}
        >
          <View
            style={{
              height: '10%',
              width: '100%'
            }}
          >
            <View style={styles.dragBar} />
          </View>
        </PanGestureHandler>

        <View style={styles.filterRowsWrapper}>
          <View style={styles.filterRow}>
            <Text style={styles.label}>Room</Text>
            <Picker
              mode="dropdown"
              itemStyle={{
                backgroundColor: '#666'
              }}
              style={{
                // backgroundColor: '#edf2fa',
                height: 30,
                minWidth: 200,
                fontSize: 14
              }}
              selectedValue={room}
              onValueChange={setRoom}
            >
              <Picker.Item color="#666" label="All" value="all" />
              {dataFilter.room.map((cat, index) => (
                <Picker.Item
                  key={index}
                  color="#666"
                  label={cat.name}
                  value={cat.value}
                />
              ))}
            </Picker>
          </View>
          <View style={styles.filterRow}>
            <Text style={styles.label}>Category</Text>
            <Picker
              mode="dropdown"
              itemStyle={{
                backgroundColor: '#666'
              }}
              style={{
                // backgroundColor: '#edf2fa',
                height: 30,
                minWidth: 200,
                fontSize: 14
              }}
              selectedValue={category}
              onValueChange={setCategory}
            >
              <Picker.Item color="#666" label="All" value="all" />
              {dataFilter.category.map((cat, index) => (
                <Picker.Item
                  key={index}
                  color="#666"
                  label={cat.name}
                  value={cat.value}
                />
              ))}
            </Picker>
          </View>
          <View style={styles.filterRow}>
            <Text style={styles.label}>Brand</Text>
            <Picker
              mode="dropdown"
              itemStyle={
                {
                  // backgroundColor: '#666',
                }
              }
              style={{
                // backgroundColor: '#edf2fa',
                height: 30,
                minWidth: 200,
                fontSize: 14
              }}
              selectedValue={brand}
              onValueChange={setBrand}
            >
              <Picker.Item color="#666" label="All" value="all" />
              {dataFilter.brand.map((cat, index) => (
                <Picker.Item
                  key={index}
                  color="#666"
                  label={cat.name}
                  value={cat.value}
                />
              ))}
            </Picker>
          </View>
          <View style={styles.filterRow}>
            <Text style={styles.label}>Color</Text>
            <Picker
              mode="dropdown"
              itemStyle={{
                backgroundColor: '#666'
              }}
              style={{
                // backgroundColor: '#edf2fa',
                height: 30,
                minWidth: 200,
                fontSize: 14
              }}
              selectedValue={color}
              onValueChange={setColor}
            >
              <Picker.Item color="#666" label="All" value="all" />
              {dataFilter.color.map((cat, index) => (
                <Picker.Item
                  key={index}
                  color="#666"
                  label={cat.name}
                  value={cat.value}
                />
              ))}
            </Picker>
          </View>

          <View>
            <View style={styles.filterRow}>
              <Text style={styles.priceValue}>
                {minPrice >= 1000000
                  ? Math.round(minPrice / 1000000) + 'tr'
                  : minPrice >= 1000
                  ? Math.round(minPrice / 1000) + 'k'
                  : minPrice}
              </Text>
              <RangeSlider
                ref={_rangeRefHandler}
                rangeEnabled={true}
                step={10000}
                min={dataFilter.minPrice}
                max={dataFilter.maxPrice}
                tintColor={'red'}
                handleBorderWidth={1}
                handleBorderColor="red"
                style={{
                  width: '80%',
                  maxWidth: '80%',
                  height: 100,
                  padding: 10
                }}
                onValueChanged={_onRangePriceChange}
              />
              <Text style={styles.priceValue}>
                {maxPrice >= 1000000
                  ? Math.round(maxPrice / 1000000) + 'tr'
                  : maxPrice >= 1000
                  ? Math.round(maxPrice / 1000) + 'k'
                  : maxPrice}
              </Text>
            </View>
            <View style={styles.recommendPrices}>
              <TouchableOpacity
                onPress={() => {
                  setMinPrice(1000000);
                  setMaxPrice(5000000);
                  rangeRef.current.setLowValue(1000000);
                  rangeRef.current.setHighValue(5000000);
                }}
                style={styles.recommendPriceItem}
              >
                <Text
                  style={{
                    fontWeight: 'bold',
                    color: '#666'
                  }}
                >
                  1M - 5M
                </Text>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => {
                  setMinPrice(5000000);
                  setMaxPrice(10000000);
                  rangeRef.current.setLowValue(5000000);
                  rangeRef.current.setHighValue(10000000);
                }}
                style={styles.recommendPriceItem}
              >
                <Text
                  style={{
                    fontWeight: 'bold',
                    color: '#666'
                  }}
                >
                  5M - 10M
                </Text>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => {
                  setMinPrice(10000000);
                  setMaxPrice(20000000);
                  rangeRef.current.setLowValue(10000000);
                  rangeRef.current.setHighValue(20000000);
                }}
                style={styles.recommendPriceItem}
              >
                <Text
                  style={{
                    fontWeight: 'bold',
                    color: '#666'
                  }}
                >
                  10M - 20M
                </Text>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => {
                  setMinPrice(20000000);
                  setMaxPrice(50000000);
                  rangeRef.current.setLowValue(10000000);
                  rangeRef.current.setHighValue(50000000);
                }}
                style={styles.recommendPriceItem}
              >
                <Text
                  style={{
                    fontWeight: 'bold',
                    color: '#666'
                  }}
                >
                  20M - 50M
                </Text>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => {
                  setMinPrice(50000000);
                  setMaxPrice(99000000);
                  rangeRef.current.setLowValue(50000000);
                  rangeRef.current.setHighValue(99000000);
                }}
                style={styles.recommendPriceItem}
              >
                <Text
                  style={{
                    fontWeight: 'bold',
                    color: '#666'
                  }}
                >
                  50M - 99M
                </Text>
              </TouchableOpacity>
            </View>
          </View>
          <TouchableOpacity onPress={_onSubmit} style={styles.btnApply}>
            <Text
              style={{
                color: '#fff',
                fontSize: 18,
                fontWeight: 'bold'
              }}
            >
              Apply Filter
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={_onReset}
            style={{
              ...styles.btnApply,
              backgroundColor: '#666',
              marginTop: 10
            }}
          >
            <Text
              style={{
                color: '#fff',
                fontSize: 18,
                fontWeight: 'bold'
              }}
            >
              Reset Filter
            </Text>
          </TouchableOpacity>
        </View>
      </Animated.View>
    </View>
  );
};

export default Filter;

const styles = StyleSheet.create({
  container: {
    position: 'relative',
    height: '100%',
    width: '100%'
  },
  backdrop: {
    backgroundColor: 'rgba(0,0,0,0.5)',
    position: 'absolute',
    top: 0,
    left: 0,
    height: '100%',
    width: '100%',
    zIndex: -1
  },
  mainFilterWrapper: {
    justifyContent: 'center',
    alignItems: 'center',
    borderTopRightRadius: 20,
    borderTopLeftRadius: 20,
    backgroundColor: '#fff',
    width: '100%',
    position: 'absolute',
    left: 0,
    height: '80%'
  },
  dragBar: {
    position: 'absolute',
    width: 70,
    height: 4,
    top: 10,
    left: (window.window.width - 70) / 2,
    backgroundColor: '#ddd',
    borderRadius: 2
  },
  filterRowsWrapper: {
    width: '100%',
    height: '90%',
    padding: 15,
    alignItems: 'center'
  },
  filterRow: {
    height: 50,
    width: '90%',
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  label: {
    fontSize: 16,
    fontWeight: 'bold'
  },
  priceValue: {
    maxWidth: '10%',
    width: '10%',
    fontSize: 14,
    textAlign: 'center',
    fontWeight: 'bold'
  },
  recommendPrices: {
    marginVertical: 10,
    width: '100%',
    flexDirection: 'row',
    flexWrap: 'wrap'
  },
  recommendPriceItem: {
    margin: 5,
    justifyContent: 'center',
    height: 40,
    paddingHorizontal: 10,
    borderStyle: 'dashed',
    borderWidth: 1,
    borderColor: '#ddd'
  },
  btnApply: {
    marginTop: 25,
    height: 44,
    borderRadius: 5,
    width: '100%',
    backgroundColor: Colors.mainColor,
    justifyContent: 'center',
    alignItems: 'center'
  }
});
