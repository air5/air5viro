import axiosInstance from '../helpers/axiosInstance';

const endpoints = {
  products: '/products/filter',
  recommendedProducts: '/products/recommend',
  product: '/products/'
};

const getProducts = (pageSize, pageNumber, filter) => {
  return axiosInstance.post(endpoints.products.concat(`?pageNumber=${pageNumber}`).concat(`&pageSize=${pageSize}`), filter);
};

const getRecommendedProducts = (pageSize, pageNumber) => {
  return axiosInstance.get(endpoints.recommendedProducts.concat(`?pageNumber=${pageNumber}`).concat(`&pageSize=${pageSize}`));
};

const getProductDetail = (id) => {
  const productsDetail = endpoints.product.concat(id);
  return axiosInstance.get(productsDetail);
};

export default {
  getProducts,
  getProductDetail,
  getRecommendedProducts
};
