import { cartActions } from '../constants/ActionTypes';
import axiosInstance from '../helpers/axiosInstance';
import { store } from '../store';
export const fetchCartProducts = () => {
    return async dispatch => {
        try {
            const products = [];
            const cartProducts = store.getState().cart.products;
            await cartProducts.map(async product => {
                const rq = await axiosInstance.get('/products/' + product.productId);
                const productData = rq.data;
                products.push({
                    ...product,
                    product: productData,
                });
                if (products.length === cartProducts.length) {
                    dispatch({
                        payload: products,
                        type: cartActions.FETCH_CART_PRODUCT_SUCCESS
                    });
                }
            });

        }
        catch (e) {
            dispatch({
                type: cartActions.FETCH_CART_PRODUCT_FAILURE
            });
        }
    };
};
export const PushCartProduct = (productId, amount) => {
    return dispatch => {
        try {
            dispatch({
                type: cartActions.PUSH_CART_PRODUCT_SUCCESS,
                payload: {
                    productId,
                    amount
                }
            });
        } catch (e) {
            dispatch({
                type: cartActions.PUSH_CART_PRODUCT_FAILURE
            });
        }
    };
};
export const RemoveCartProduct = (productId) => {
    return dispatch => {
        try {
            dispatch({
                type: cartActions.REMOVE_CART_PRODUCT_SUCCESS,
                payload: {
                    productId,
                }
            });
        } catch (e) {
            dispatch({
                type: cartActions.REMOVE_CART_PRODUCT_FAILURE
            });
        }
    };
};